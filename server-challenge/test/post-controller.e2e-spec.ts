import * as mongoose from 'mongoose';
import { app } from './constants';
import * as request from 'supertest';
import axios from 'axios';

beforeAll(async () => {
    await mongoose.connect('mongodb://mongo/fullstack-db');
    await mongoose.connection.db.dropDatabase();
});

afterAll(async () => {
    await mongoose.disconnect();
});

describe('POST', () => {
  
    it('should get all post', () => {
        return request(app)
        .get('post')
        .set('Accept', 'application/json')
        .expect(200);
    });

});