import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { app } from './constants';


describe('ROOT', () => {

  it('/ (GET)', () => {
    return  request('http://localhost:3000').get('/')
    .expect(200)
    .expect('Hello World!');
  });
});
