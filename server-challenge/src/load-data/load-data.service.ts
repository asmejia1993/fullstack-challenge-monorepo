import { Injectable, OnModuleInit, HttpService } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Cron, CronExpression } from '@nestjs/schedule';
import { Model } from 'mongoose';
import { CreatePostDto } from 'src/post/dto/create-post.dto';
import { Post } from 'src/post/interface/Post';
import { DateTime } from 'luxon';

@Injectable()
export class LoadDataService implements OnModuleInit {
    
    constructor(@InjectModel('Post') private readonly postModel: Model<Post>,private httpService: HttpService) {}

    initPosts: CreatePostDto[]  = [];
    count: number = 0;

    async onModuleInit() {
        console.log('The module has been initialized.');
        this.reloadDataEvery1Hour();  
    }
    
    @Cron(CronExpression.EVERY_HOUR)
    async reloadDataEvery1Hour() {  
        try {
            const result = await this.httpService.get('http://hn.algolia.com/api/v1/search_by_date?query=nodejs').toPromise();
            const response = result.data.hits;
            for (const value of response) { 
                const dto = new CreatePostDto(
                                        value.title || value.story_title,
                                        value.author, 
                                        value.created_at, 
                                        this.parseDate(value.created_at), 
                                        value.story_url || value.url, 
                                        true, 
                                        value.story_id); 
                const newPost = new this.postModel(dto);
                const exist = await this.postModel.exists({created_at: newPost.created_at, author: newPost.author, title: newPost.title});
                if (!exist) {
                    await newPost.save();
                }
           }
        } catch (error) {
            console.error("error on reloadDataEvery1Hour method: ", error);
        }
    }


    parseDate(value: string) : string{
        
        const d = DateTime.fromISO(value);

        let timeFormatted: string = d.toFormat('t').toLowerCase();
        const days = Math.abs(Math.round(d.diffNow('days').days));

        if (days == 1) {
            timeFormatted = 'Yesterday';
        } else if (days > 1) {
            timeFormatted = d.toFormat('DD').split(',')[0];
        } 
        return timeFormatted;
    }
}
