import { HttpModule, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PostSchema } from 'src/post/schema/post.schema';
import { LoadDataService } from './load-data.service';

@Module({
    imports: [MongooseModule.forFeature(
        [
            { name: 'Post', schema: PostSchema }
        ]), HttpModule],
    providers: [LoadDataService]
})
export class LoadDataModule {
    
}
