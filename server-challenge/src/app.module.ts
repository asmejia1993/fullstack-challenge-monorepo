import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { PostModule } from './post/post.module';
import { ScheduleModule } from '@nestjs/schedule';
import { LoadDataModule } from './load-data/load-data.module';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://mongo/fullstack-db'),
    ScheduleModule.forRoot(),
    PostModule,
    LoadDataModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
