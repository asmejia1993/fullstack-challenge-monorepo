export class CreatePostDto {
    title: string;
    author: string;
    created_at: string;
    url: string;
    hide: boolean;
    time: string;
    story_id: string;

    constructor(title: string, author: string, created_at: string, time: string, url: string, hide: boolean, story_id:string ) {
        this.title = title;
        this.author = author;
        this.hide = hide;
        this.created_at = created_at;
        this.time = time;
        this.url = url;
        this.story_id = story_id
    }
}