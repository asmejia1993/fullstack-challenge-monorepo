import { Schema } from 'mongoose';

export const PostSchema = new Schema({
    title: String,
    author: String,
    created_at: String,
    time: String,
    url:String,
    hide: Boolean,
    story_id: String
});


