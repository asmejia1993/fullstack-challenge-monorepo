import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { PostService } from './post.service';

@Controller('v1/post')
export class PostController {

    postInit: [];

    constructor(private postService: PostService) {
        
    }

    @Get()
    getPosts() {
        return this.postService.getPosts();
    }

    @Delete(':id')
    removePost(@Param('id') id: string) {
        this.postService.removePost(id);
        return this.postService.getPosts();
    }

}
