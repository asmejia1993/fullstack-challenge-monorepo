import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/internal/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, of} from 'rxjs';
import { Post } from '../components/post/post.model';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  private readonly URL = 'http://localhost:3000/v1/post';

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  getPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(this.URL)
  }

  removePost(id: String): Observable<any> {
      const partialUrl = `${this.URL}/${id}`;
    
      return this.http.delete<any>(partialUrl, this.httpOptions).pipe(
        catchError(this.handleError<any>('deletePost'))
      );
  }
}
