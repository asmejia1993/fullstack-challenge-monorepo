# Monorepo app using simple API(Nestjs, Typescript, Mongoose), Angular latest version and Yarn Workspace

This is a boilerplate created to build modular and scalable projects. It contains a template for both projects.

It uses OpenAPI to document all API endpoints - (see [server-challenge-readme.][back-readme])


### Tech

This app uses a number of open source projects to work properly:

* [Angular] - HTML enhanced for web apps!
* [Node.js] - evented I/O for the backend
* [Nestjs] - A progressive Node.js framework for building efficient, reliable and scalable server-side applications


### Docker container
This app is very easy to install and deploy in a Docker container usign docker-compose file to managment all of images.

To deploy images requires execute the next commands:
```sh
git clone https://gitlab.com/asmejia1993/fullstack-challenge-monorepo.git
cd fullstack-challenge-monorepo
docker-compose build
docker-compose up
```
This will create the app image and pull in the necessary dependencies.

Once done, the deployment by navigating to your server address in your preferred browser.
```sh
Go to http://localhost:8081 to check out the app client
```
The API URL are:
```sh
http://localhost:3000/v1/post        Method:GET, This get the recents post
http://localhost:3000/v1/post/id     Method:DELETE, This remove an post from database
```

   [back-readme]: <https://gitlab.com/asmejia1993/fullstack-challenge-monorepo/-/blob/master/server-challenge/README.md>
   [Node.js]: <http://nodejs.org>
   [Nestjs]: <https://nestjs.com>
   [Angular]: <https://angular.io>

